age = input("What is your current age? ")

# 1 year = 365 days, 52 weeks, 12 months

ninety_days = 32850
ninety_weeks = 4680
ninety_months = 1080

current_days = int(age) * 365
current_weeks = int(age) * 52
current_months = int(age) * 12

days_left = ninety_days - current_days
weeks_left = ninety_weeks - current_weeks
months_left = ninety_months - current_months

print(f"You have {days_left} days, {weeks_left} weeks, and {months_left} months left.")

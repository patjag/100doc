name1 = input("What is your name? \n").lower()
name2 = input("What is their name? \n").lower()
names = name1 + name2

number1 = names.count("t") + names.count("r") + names.count("u") + names.count("e") #TRUE occurrence
print(number1)
number2 = names.count("l") + names.count("o") + names.count("v") + names.count("e") #LOVE occurrence
print(number2)
score = int(str(number1) + str(number2))

if score < 10 or score > 90:
    print(f"Your score is {score}, you are terrible together!")
elif score > 40 and score < 50:
    print(f"Your score is {score}, you are alright together!")
else:
    print(f"Your score is {score}")
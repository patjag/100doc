bill = float(input("What was the bill? "))
tip = int(input("What percentage bill would you like to give? 10, 12, or 15? "))
people = int(input("How many people to split the bill? "))

tip_percentage = tip/100
total_tip = bill * tip_percentage
total_bill = total_tip + bill
bill_divided = round((total_bill / people), 2)
print(f"Each person should pay {bill_divided} dollars")


